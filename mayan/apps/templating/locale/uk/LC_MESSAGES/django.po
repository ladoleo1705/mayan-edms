# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Сергій Гарашко, 2024
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-07 07:20+0000\n"
"PO-Revision-Date: 2024-01-02 18:58+0000\n"
"Last-Translator: Сергій Гарашко, 2024\n"
"Language-Team: Ukrainian (https://app.transifex.com/rosarior/teams/13584/uk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: uk\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);\n"

#: apps.py:19 permissions.py:6
msgid "Templating"
msgstr ""

#: fields.py:21
#, python-format
msgid ""
"Use Django's default templating language "
"(https://docs.djangoproject.com/en/%(django_version)s/ref/templates/builtins/)."
" "
msgstr ""

#: fields.py:41
#, python-format
msgid ""
"Use Django's default templating language "
"(https://docs.djangoproject.com/en/%(django_version)s/ref/templates/builtins/)."
" The {{ %(variable)s }} variable is available to the template."
msgstr ""

#: forms.py:9
msgid "Resulting text from the evaluated template."
msgstr ""

#: forms.py:10
msgid "Result"
msgstr ""

#: forms.py:20
msgid "The template string to be evaluated."
msgstr ""

#: forms.py:21
msgid "Template"
msgstr "Шаблон"

#: links.py:11
msgid "Sandbox"
msgstr ""

#: permissions.py:10
msgid "Use the template sandbox"
msgstr ""

#: serializers.py:8
msgid "Hex hash"
msgstr ""

#: serializers.py:11
msgid "Name"
msgstr ""

#: serializers.py:14
msgid "HTML"
msgstr ""

#: serializers.py:17
msgid "URL"
msgstr ""

#: views.py:44
#, python-format
msgid "Template sandbox for: %s"
msgstr ""

#: views.py:65
#, python-format
msgid "Template error; %(exception)s"
msgstr ""

#: widgets.py:69
msgid "Filters"
msgstr ""

#: widgets.py:76
msgid "Tags"
msgstr ""

#: widgets.py:83
msgid "<Filters and tags>"
msgstr ""

#: widgets.py:128
msgid "<Model attributes>"
msgstr ""
